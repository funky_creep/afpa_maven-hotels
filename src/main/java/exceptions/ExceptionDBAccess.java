package exceptions;

import model.utils.JournalLog;

public class ExceptionDBAccess extends ExceptionHandler {

	private static final long serialVersionUID = 1771578314721869334L;

	public ExceptionDBAccess(String message) {
		super(message, JournalLog.ERROR);
	}
	
}
