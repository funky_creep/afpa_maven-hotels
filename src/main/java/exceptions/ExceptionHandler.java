package exceptions;

import model.utils.JournalLog;

public class ExceptionHandler extends Exception {

	private static final long serialVersionUID = 7506055338527152123L;

	public ExceptionHandler(String message, int level) {
		JournalLog.addMessage(level, message);
		if (level == JournalLog.ERROR) {
			System.out.println(message);
		}
	}
}
