package model.beans;

import model.model.Collectionnable;

public class Hotel extends Collectionnable {

	private TypeHotel typeHotel;
	private String nomHotel;
	private String adresseHotel;
	private String cpHotel;
	private String villeHotel;
	
	public Hotel(int nuHotel, TypeHotel typeHotel, String nomHotel, String adresseHotel, String cpHotel,
			String villeHotel) {
		super(nuHotel, nomHotel);
		this.typeHotel = typeHotel;
		this.nomHotel = nomHotel;
		this.adresseHotel = adresseHotel;
		this.cpHotel = cpHotel;
		this.villeHotel = villeHotel;
	}

	public TypeHotel getTypeHotel() {
		return typeHotel;
	}
	
	public String getNomHotel() {
		return nomHotel;
	}

	public void setNomHotel(String nomHotel) {
		this.nomHotel = nomHotel;
	}

	public String getAdresseHotel() {
		return adresseHotel;
	}

	public void setAdresseHotel(String adresseHotel) {
		this.adresseHotel = adresseHotel;
	}

	public String getCpHotel() {
		return cpHotel;
	}

	public void setCpHotel(String cpHotel) {
		this.cpHotel = cpHotel;
	}

	public String getVilleHotel() {
		return villeHotel;
	}

	public void setVilleHotel(String villeHotel) {
		this.villeHotel = villeHotel;
	}

}
