package model.beans;

import model.model.Collectionnable;

public class TypeHotel extends Collectionnable {

	private String nomType;
	
	public TypeHotel(int nuType, String nomType) {
		super(nuType, nomType);
		this.nomType = nomType;
	}

	public String getNomType() {
		return nomType;
	}

	public void setNomType(String nomType) {
		this.nomType = nomType;
	}

}
