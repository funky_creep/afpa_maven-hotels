package model.beans;

import model.model.Collectionnable;

public class Categorie extends Collectionnable {

	private int nuCategorie;
	private String nomCategorie;
	
	public Categorie(int nuCategorie, String nomCategorie) {
		super(nuCategorie, nomCategorie);
		this.nomCategorie = nomCategorie;
	}

	public void setNuCategorie(int nuCategorie) {
		this.nuCategorie = nuCategorie;
	}
	
	public int getNuCategorie() {
		return nuCategorie;
	}

	public String getNomCategorie() {
		return nomCategorie;
	}

	public void setNomCategorie(String nomCategorie) {
		this.nomCategorie = nomCategorie;
	}
	
}
