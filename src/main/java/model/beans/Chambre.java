package model.beans;

import model.model.Collectionnable;

public class Chambre extends Collectionnable {

	private int nuChambre;
	private Categorie categorie;
	
	public Chambre(int nuChambre, Categorie categorie, int nuHotel) {
		// l'id d'une chambre est la concaténation du numéro de la chambre et du numéro de l'hotel
		super(Integer.parseInt(Integer.toString(nuChambre)+Integer.toString(nuHotel)), Integer.toString(nuChambre)+Integer.toString(nuHotel));
		this.nuChambre = nuChambre;
		this.categorie = categorie;
	}
	
	public Categorie getCategorie() {
		return categorie;
	}
	
	public int getNuChambre() {
		return nuChambre;
	}

	public void setNuChambre(int nuChambre) {
		this.nuChambre = nuChambre;
	}
	
}
