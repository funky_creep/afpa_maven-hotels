package model.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import exceptions.ExceptionDBAccess;

public class Connexion {

	private static final String ADDRESS = "jdbc:postgresql://localhost:5432/hotels";
	private static final String LOGIN = "postgres";
	private static final String PASSWORD = "postgres";
	
	private static Connection connection;
	
	// Constructeur priv�
	private Connexion() throws ExceptionDBAccess {
		try {
			Class.forName("org.postgresql.Driver"); // rajout�
			connection = DriverManager.getConnection(ADDRESS, LOGIN, PASSWORD);
		} catch (SQLException e) {
			throw new ExceptionDBAccess("Probl�me de connexion � la base de donn�es");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static Connection getInstance() throws ExceptionDBAccess {
		try {
			if (connection == null || !connection.isValid(0)) {
				new Connexion();
			}
		} catch (SQLException e) {
			throw new ExceptionDBAccess("Timeout ou connexion perdue");
		}
		return connection;
	}
	
	public static void closeConnection() {
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
