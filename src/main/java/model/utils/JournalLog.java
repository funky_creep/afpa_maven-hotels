package model.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class JournalLog {

	private static final Logger LOGGER = LogManager.getLogger(JournalLog.class);
	public static final int INFO = 0;
	public static final int WATCHOUT = 1;
	public static final int ERROR = 2;
	
	public static void addMessage(int level, String message) {
		switch(level) {
			case INFO:
				LOGGER.debug(message);
				break;
			case WATCHOUT:
				LOGGER.warn(message);
				break;
			case ERROR:
				LOGGER.error(message);
				break;
			default:
				break;
		}
	}
}
