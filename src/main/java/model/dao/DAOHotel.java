package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import exceptions.ExceptionDBAccess;
import exceptions.ExceptionRequest;
import model.beans.Hotel;
import model.model.Collectionneur;

/**
 * DAO correspondant � la table Hotel de la base de donn�es, h�rite de la classe g�n�rique DAO
 * @author Funky_Creep
 *
 */
public class DAOHotel extends DAO<Hotel>{
	
	/**
	 * Instance du DAOTypeHotel qui nous permet de r�cup�rer un objet TypeHotel � partir d'un num�ro de type d'h�tel
	 */
	private DAOTypeHotel daotypehotel = new DAOTypeHotel();
	
	//private String reqFindByRoomCat = "SELECT DISTINCT ON(nuhotel) * FROM hotel INNER JOIN chambre USING(nuhotel) WHERE nucatchambre IN (";
	
	/**
	 * Constructeur DAOHotel contenant les diff�rentes requ�tes insert, update, delete, findById et findAll
	 */
	public DAOHotel() {
		reqInsert = "INSERT INTO hotel VALUES(?, ?, ?, ?, ?, ?)";
		reqUpdate = "UPDATE hotel SET nutypehotel=?, nomhotel=?, adressehotel=?, cphotel=?, villehotel=? WHERE nuhotel=?";
		reqDelete = "DELETE FROM hotel WHERE nuhotel=?";
		reqFindById = "SELECT * FROM hotel WHERE nuhotel=?";
		reqFindAll = "SELECT * FROM hotel";
	}
	
	@Override
	public int executeQuery(PreparedStatement pStmt, Hotel obj, String request) throws ExceptionRequest {
		if (request == "insert") {
			try {
				pStmt.setInt(1, obj.getId());
				pStmt.setInt(2, obj.getTypeHotel().getId());
				pStmt.setString(3, obj.getNomHotel());
				pStmt.setString(4, obj.getAdresseHotel());
				pStmt.setString(5, obj.getCpHotel());
				pStmt.setString(6, obj.getVilleHotel());
				return pStmt.executeUpdate();
			} catch (SQLException e) {
				throw new ExceptionRequest("Impossible d'ins�rer cet h�tel");
			}
		}
		else {
			try {
				pStmt.setInt(1, obj.getTypeHotel().getId());
				pStmt.setString(2, obj.getNomHotel());
				pStmt.setString(3,  obj.getAdresseHotel());
				pStmt.setString(4, obj.getCpHotel());
				pStmt.setString(5,  obj.getVilleHotel());
				pStmt.setInt(6,  obj.getId());
				return pStmt.executeUpdate();
			} catch (SQLException e) {
				throw new ExceptionRequest("Impossible de modifier cet h�tel");
			}
			
		}
	}
	
	@Override
	public Optional<Hotel> resultSetToOptional(ResultSet rs) throws ExceptionRequest {
		try {
			return Optional.ofNullable(new Hotel(rs.getInt("nuhotel"), daotypehotel.findById(rs.getInt("nutypehotel")).get(), rs.getString("nomhotel"), rs.getString("adressehotel"), rs.getString("cphotel"), rs.getString("villehotel")));
		} catch (SQLException e) {
			throw new ExceptionRequest("Impossible d'acc�der � cet h�tel");
		} catch (ExceptionDBAccess e) {
			return null;
		}
	}
	
	/*public Collectionneur<Hotel> findByRoomCat(int nucatchambre) throws ExceptionDBAccess, ExceptionRequest {
		Collectionneur<Hotel> hotels = new Collectionneur<Hotel>();
		try {
			PreparedStatement pStmt = getConnection().prepareStatement(reqFindByRoomCat);
			pStmt.setInt(1, nucatchambre);
			ResultSet rs = pStmt.executeQuery();
			while (rs.next()) {
				Optional<Hotel> optionalBuiltFromRs = resultSetToOptional(rs);
				hotels.addItem(optionalBuiltFromRs.get());
			}
			
			//JournalLog.addMessage(JournalLog.INFO, this.getClass().getName()+"s trouv�(es)");
			return hotels;
		} catch (SQLException e) {
			throw new ExceptionRequest("Impossible de trouver les h�tels");
		}
	}*/
	
	/*public Collectionneur<Hotel> findByTypeHotel(String[] nutypeshotel) {
		String reqFindByRoomCat = "SELECT DISTINCT ON(nuhotel) * FROM hotel INNER JOIN chambre USING(nuhotel) WHERE nucatchambre IN (";
		Collectionneur<Hotel> hotels = new Collectionneur<Hotel>();
		PreparedStatement pStmt = getConnection().prepareStatement(reqFindByRoomCat);
		ResultSet rs = pStmt.executeQuery();
		while (rs.next()) {
			Optional<Hotel> optionalBuiltFromRs = resultSetToOptional(rs);
			hotels.addItem(optionalBuiltFromRs.get());
		}
		//JournalLog.addMessage(JournalLog.INFO, this.getClass().getName()+"s trouv�(es)");
		return hotels;
	}*/
	
	public Collectionneur<Hotel> findByRoomCat(String[] nucatchambres) throws ExceptionDBAccess, ExceptionRequest {
		String reqFindByRoomCat = "SELECT DISTINCT ON(nuhotel) * FROM hotel INNER JOIN chambre USING(nuhotel) WHERE nucatchambre IN (";
		Collectionneur<Hotel> hotels = new Collectionneur<Hotel>();
		
		if (nucatchambres.length != 0) {
			try {
				for (int i=0; i<nucatchambres.length; i++) {
					if (i < nucatchambres.length-1) {
						reqFindByRoomCat += Integer.parseInt(nucatchambres[i]) + ",";
					}
					else {
						reqFindByRoomCat += Integer.parseInt(nucatchambres[i]);
					}
				}
				reqFindByRoomCat += ")";
				//System.out.println(reqFindByRoomCat);
				PreparedStatement pStmt = getConnection().prepareStatement(reqFindByRoomCat);
				ResultSet rs = pStmt.executeQuery();
				while (rs.next()) {
					Optional<Hotel> optionalBuiltFromRs = resultSetToOptional(rs);
					hotels.addItem(optionalBuiltFromRs.get());
				}
				//JournalLog.addMessage(JournalLog.INFO, this.getClass().getName()+"s trouv�(es)");
				return hotels;
			} catch (SQLException e) {
				throw new ExceptionRequest("Impossible de trouver les h�tels - ROOMCAT");
			}
		}
		return hotels;
	}

}
