package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import exceptions.ExceptionDBAccess;
import exceptions.ExceptionRequest;
import model.beans.Categorie;
import model.beans.Chambre;
import model.utils.JournalLog;

/**
 * DAO correspondant � la table Categorie de la base de donn�es, h�rite de la classe g�n�rique DAO
 * @author Funky_Creep
 *
 */
public class DAOChambre extends DAO<Chambre>{
	
	/**
	 * Instance du DAOCategorie qui nous permet de r�cup�rer un objet Categorie (de chambre) � partir de son id
	 */
	private DAOCategorie daocategorie = new DAOCategorie();
	
	/**
	 * Constructeur DAOChambre contenant les diff�rentes requ�tes insert, update, delete, findById et findAll
	 */
	public DAOChambre() {
		reqInsert = "INSERT INTO chambre VALUES(?, ?, ?)";
		reqUpdate = "UPDATE chambre SET nucatchambre=? WHERE nuchambre=? AND nuhotel=?";
		reqDelete = "DELETE FROM chambre WHERE nuchambre=? AND nuhotel=?";
		reqFindById = "SELECT * FROM chambre WHERE nuchambre=? AND nuhotel=?";
		reqFindAll = "SELECT * FROM chambre";
	}
	
	@Override
	public int executeQuery(PreparedStatement pStmt, Chambre obj, String request) throws ExceptionRequest {
		if (request == "insert") {
			throw new ExceptionRequest("Impossible d'ins�rer une chambre sans un h�tel");
		}
		else {
			throw new ExceptionRequest("Impossible de modifier une chambre sans un h�tel");
		}
	}
	
	@Override
	public Optional<Chambre> resultSetToOptional(ResultSet rs) throws ExceptionRequest {
		try {
			return Optional.ofNullable(new Chambre(rs.getInt("nuchambre"), daocategorie.findById(rs.getInt("nucatchambre")).get(), rs.getInt("nuhotel")));
		} catch (SQLException e) {
			throw new ExceptionRequest("Impossible d'acc�der � cette chambre");
		} catch (ExceptionDBAccess e) {
			return null;
		}
	}
	
	public Chambre insert(Chambre objACreer, int nuHotel) throws ExceptionRequest, ExceptionDBAccess {
		try {
			PreparedStatement pStmt = getConnection().prepareStatement(reqInsert);
			pStmt.setInt(1, objACreer.getNuChambre());
			pStmt.setInt(2, objACreer.getCategorie().getId());
			pStmt.setInt(3, nuHotel);
			
			pStmt.executeUpdate();
			
			JournalLog.addMessage(JournalLog.INFO, "Nouvelle chambre ins�r�e");
			return objACreer;
		} catch (SQLException e) {
			throw new ExceptionRequest("Impossible d'ins�rer cette chambre");
		}
	}
	
	public Chambre update(Chambre objAModifier, int nuHotel) throws ExceptionRequest, ExceptionDBAccess {
		try {
			PreparedStatement pStmt = getConnection().prepareStatement(reqUpdate);
			pStmt.setInt(1, objAModifier.getCategorie().getNuCategorie());
			pStmt.setInt(2, objAModifier.getNuChambre());
			pStmt.setInt(3, nuHotel);

			pStmt.executeUpdate();

			JournalLog.addMessage(JournalLog.INFO, "Chambre(s) modifi�e(s)");
			return objAModifier;
		} catch (SQLException e) {
			throw new ExceptionRequest("Impossible de modifier cette Chambre");
		}
	}

	public void delete(Chambre chambreASupprimer, int nuHotel) throws ExceptionRequest, ExceptionDBAccess {
		try {
			PreparedStatement pStmt = getConnection().prepareStatement(reqDelete);
			pStmt.setInt(1, chambreASupprimer.getNuChambre());
			pStmt.setInt(2, nuHotel);
			
			pStmt.executeUpdate();
			JournalLog.addMessage(JournalLog.INFO, "Chambre(s) supprim�e(s)");
		} catch (SQLException e) {
			throw new ExceptionRequest("Impossible de supprimer cette Chambre");
		}
	}
	
	public Chambre findById(int nuChambre, int nuHotel) throws ExceptionRequest, ExceptionDBAccess {
		try {
			PreparedStatement pStmt = getConnection().prepareStatement(reqFindById);
			pStmt.setInt(1, nuChambre);
			pStmt.setInt(2, nuHotel);
			
			ResultSet rs = pStmt.executeQuery();
			rs.next();
			
			JournalLog.addMessage(JournalLog.INFO, "Chambre trouv�e");
			return new Chambre(rs.getInt("nuchambre"), daocategorie.findById(rs.getInt("nucatchambre")).get(), rs.getInt("nuhotel"));
		} catch (SQLException e) {
			throw new ExceptionRequest("Impossible de trouver cet(te) "+this.getClass().getName());
		}
	}

}
