package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import exceptions.ExceptionRequest;
import model.beans.Hotel;
import model.beans.TypeHotel;
import model.utils.Connexion;

/**
 * DAO correspondant � la table TypeHotel de la base de donn�es, h�rite de la classe g�n�rique DAO
 * @author Funky_Creep
 *
 */
public class DAOTypeHotel extends DAO<TypeHotel>{
	
	/**
	 * Constructeur DAOTypeHotel contenant les diff�rentes requ�tes insert, update, delete, findById et findAll
	 */
	public DAOTypeHotel() {
		reqInsert = "INSERT INTO type_hotel VALUES(?, ?)";
		reqUpdate = "UPDATE type_hotel SET nomtype=? WHERE nutype=?";
		reqDelete = "DELETE FROM type_hotel WHERE nutype=?";
		reqFindById = "SELECT * FROM type_hotel WHERE nutype=?";
		reqFindAll = "SELECT * FROM type_hotel";
	}
	
	@Override
	public int executeQuery(PreparedStatement pStmt, TypeHotel obj, String request) throws ExceptionRequest {
		if (request == "insert") {
			try {
				pStmt.setInt(1, obj.getId());
				pStmt.setString(2, obj.getNomType());
				return pStmt.executeUpdate();
			} catch (SQLException e) {
				throw new ExceptionRequest("Impossible d'ins�rer ce type d'h�tel");
			}
		}
		else {
			try {
				pStmt.setString(1, obj.getNomType());
				pStmt.setInt(2, obj.getId());
				return pStmt.executeUpdate();
			} catch (SQLException e) {
				throw new ExceptionRequest("Impossible de modifier ce type d'h�tel");
			}
			
		}
	}
	
	@Override
	public Optional<TypeHotel> resultSetToOptional(ResultSet rs) throws ExceptionRequest {
		try {
			return Optional.ofNullable(new TypeHotel(rs.getInt("nutype"), rs.getString("nomtype")));
		} catch (SQLException e) {
			throw new ExceptionRequest("Impossible d'acc�der � ce type d'h�tel");
		}
	}

}
