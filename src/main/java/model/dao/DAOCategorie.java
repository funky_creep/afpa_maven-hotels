package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import exceptions.ExceptionRequest;
import model.beans.Categorie;

/**
 * DAO correspondant � la table Categorie de la base de donn�es, h�rite de la classe g�n�rique DAO
 * @author Funky_Creep
 *
 */
public class DAOCategorie extends DAO<Categorie>{
	
	/**
	 * Constructeur DAOCategorie contenant les diff�rentes requ�tes insert, update, delete, findById et findAll
	 */
	public DAOCategorie() {
		reqInsert = "INSERT INTO categorie VALUES(?, ?)";
		reqUpdate = "UPDATE categorie SET nomcat=? WHERE nucat=?";
		reqDelete = "DELETE FROM categorie WHERE nucat=?";
		reqFindById = "SELECT * FROM categorie WHERE nucat=?";
		reqFindAll = "SELECT * FROM categorie";
	}
	
	@Override
	public int executeQuery(PreparedStatement pStmt, Categorie obj, String request) throws ExceptionRequest {
		if (request == "insert") {
			try {
				pStmt.setInt(1, obj.getId());
				pStmt.setString(2, obj.getNomCategorie());
				return pStmt.executeUpdate();
			} catch (SQLException e) {
				throw new ExceptionRequest("Impossible d'ins�rer cette cat�gorie");
			}
		}
		else {
			try {
				pStmt.setString(1, obj.getNomCategorie());
				pStmt.setInt(2, obj.getNuCategorie());
				return pStmt.executeUpdate();
			} catch (SQLException e) {
				throw new ExceptionRequest("Impossible de modifier cette cat�gorie");
			}
			
		}
	}
	
	@Override
	public Optional<Categorie> resultSetToOptional(ResultSet rs) throws ExceptionRequest {
		try {
			return Optional.ofNullable(new Categorie(rs.getInt("nucat"), rs.getString("nomcat")));
		} catch (SQLException e) {
			throw new ExceptionRequest("Impossible d'acc�der � cette cat�gorie");
		}
	}
	
}
