package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import exceptions.ExceptionDBAccess;
import exceptions.ExceptionRequest;
import model.model.Collectionneur;
import model.model.Collectionnable;
import model.utils.Connexion;
import model.utils.JournalLog;

/**
 * <p>DAO est la classe abstraite <b>g�n�rique</b> permettant d'acc�der aux donn�es de la base de donn�es. </p>
 * @author Funky_Creep
 *
 * @param <T> Une table de la base de donn�es
 */
public abstract class DAO<T extends Collectionnable> {

	/**
	 * Requ�te d'insertion d'un �lement dans la table
	 */
	protected String reqInsert;
	
	/**
	 * Requ�te de modification d'un �l�ment dans la table
	 */
	protected String reqUpdate;
	
	/**
	 * Requ�te de suppression d'un �l�ment dans la table
	 */
	protected String reqDelete;
	
	/**
	 * Requ�te de s�lection d'un �lement de la table
	 */
	protected String reqFindById;
	
	/**
	 * Requ�te de s�lection de tous les �l�ments de la table
	 */
	protected String reqFindAll;
	
	/**
	 * R�cup�re une instance de connexion
	 * @return Une instance de connexion
	 * @throws ExceptionDBAccess Si la connexion � la base de donn�es ne fonctionne pas
	 */
	protected Connection getConnection() throws ExceptionDBAccess {
		return Connexion.getInstance();
	}
	
	/**
	 * M�thode abstraite qui transforme un ResultSet en objet container contenant (ou non) l'�l�ment
	 * @param rs Un Resultset retourn� par une requ�te 
	 * @return Un objet container contenant (ou non) un �lement
	 * @throws ExceptionRequest Si la requ�te ne s'ex�cute pas
	 */
	public abstract Optional<T> resultSetToOptional(ResultSet rs) throws ExceptionRequest;
	
	/**
	 * M�thode abstraite qui ex�cute une requ�te insert ou update, en fonction du param�tre request
	 * @param ps PreparedStatement contenant la requ�te � ex�cuter
	 * @param objACreer �l�ment � ins�rer ou modifier dans la base de donn�es
	 * @param request Cha�ne de caract�res contenant la requ�te: "insert" ou "update"
	 * @return Un entier r�sultant de l'ex�cution de la requ�te
	 * @throws ExceptionRequest Si la requ�te ne s'ex�cute pas
	 */
	public abstract int executeQuery(PreparedStatement ps, T objACreer, String request) throws ExceptionRequest;
	
	/**
	 * Ins�re un �l�ment dans la base de donn�es
	 * @param objACreer l'�l�ment � ins�rer dans la base de donn�es
	 * @return l'�l�ment ins�r�
	 * @throws ExceptionRequest Si la requ�te ne s'ex�cute pas
	 * @throws ExceptionDBAccess Si la connexion � la base de donn�es est impossible
	 */
	public Optional<T> insert(T objACreer) throws ExceptionRequest, ExceptionDBAccess {
		try {
			PreparedStatement pStmt = getConnection().prepareStatement(reqInsert);
			
			// Appelle la m�thode executeQuery des DAO
			executeQuery(pStmt, objACreer, "insert");
			
			JournalLog.addMessage(JournalLog.INFO, "Nouveau(elle) "+this.getClass().getName()+" ins�r�(e)");
			return findById(objACreer.getId());
		} catch (SQLException e) {
			throw new ExceptionRequest("Impossible d'ins�rer cet(te) "+this.getClass().getName());
		}
	}
	
	/**
	 * Met � jour un �l�ment dans la base de donn�es
	 * @param objAModifier l'�lement � modifier dans la base de donn�es
	 * @return l'�l�ment modifi�
	 * @throws ExceptionRequest Si la requ�te ne s'ex�cute pas
	 * @throws ExceptionDBAccess Si la connexion � la base de donn�es est impossible
	 */
	public Optional<T> update(T objAModifier) throws ExceptionRequest, ExceptionDBAccess {
		try {
			PreparedStatement pStmt = getConnection().prepareStatement(reqUpdate);
			
			// Appelle la m�thode executeQuery des DAO
			executeQuery(pStmt, objAModifier, "update");
			
			pStmt.executeUpdate();
			
			JournalLog.addMessage(JournalLog.INFO, this.getClass().getName()+" modifi�(e)");
			return findById(objAModifier.getId());
		} catch (SQLException e) {
			throw new ExceptionRequest("Impossible de modifier ce(tte) "+this.getClass().getName());
		}
	}
	
	/**
	 * Supprime un �l�ment de la base de donn�es
	 * @param idASupprimer l'id de l'�l�ment � supprimer dans la base de donn�es
	 * @throws ExceptionRequest Si la requ�te ne s'ex�cute pas
	 * @throws ExceptionDBAccess Si la connexion � la base de donn�es est impossible
	 */
	public void delete(int idASupprimer) throws ExceptionRequest, ExceptionDBAccess {
		try {
			PreparedStatement pStmt = getConnection().prepareStatement(reqDelete);
			pStmt.setInt(1, idASupprimer);
			
			pStmt.executeUpdate();
			JournalLog.addMessage(JournalLog.INFO, this.getClass().getName()+" supprim�(e)");
		} catch (SQLException e) {
			throw new ExceptionRequest("Impossible de modifier ce(tte) "+this.getClass().getName());
		}
	}
	
	/**
	 * R�cup�re un �l�ment de la table dans la base de donn�es
	 * @param id l'id de l'�l�ment � trouver dans la base de donn�es
	 * @return Un objet container qui contient (ou non) l'�l�ment recherch� dans la base de donn�es
	 * @throws ExceptionRequest Si la requ�te ne s'ex�cute pas
	 * @throws ExceptionDBAccess Si la connexion � la base de donn�es est impossible
	 */
	public Optional<T> findById(int id) throws ExceptionRequest, ExceptionDBAccess {
		PreparedStatement pStmt = null;
		try {
			pStmt = getConnection().prepareStatement(reqFindById);
			pStmt.setInt(1, id);
			
			ResultSet rs = pStmt.executeQuery();
			rs.next();
			
			//JournalLog.addMessage(JournalLog.INFO, this.getClass().getName() + " trouv�(e)");
			Optional<T> optionalBuiltFromRs = resultSetToOptional(rs);
			if (optionalBuiltFromRs.isPresent()) {
				return optionalBuiltFromRs;
			}
			rs.close();
		} catch (SQLException e) {
			throw new ExceptionRequest("Impossible de trouver cet(te) "+this.getClass().getName());
		}
		finally {
			try {
				pStmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return Optional.empty();
	}
	
	/**
	 * R�cup�re tous les �l�ments de la table dans la base de donn�es
	 * @return Une collection contenant les �lements de la table 
	 * @throws ExceptionRequest Si la requ�te ne s'ex�cute pas
	 * @throws ExceptionDBAccess Si la connexion � la base de donn�es est impossible
	 */
	public Collectionneur<T> findAll() throws ExceptionRequest, ExceptionDBAccess {
		Collectionneur<T> collectionneur = new Collectionneur<T>();
		try {
			PreparedStatement pStmt = getConnection().prepareStatement(reqFindAll);
			
			ResultSet rs = pStmt.executeQuery();
			while (rs.next()) {
				Optional<T> optionalBuiltFromRs = resultSetToOptional(rs);
				collectionneur.addItem(optionalBuiltFromRs.get());
			}
			
			//JournalLog.addMessage(JournalLog.INFO, this.getClass().getName()+"s trouv�(es)");
			return collectionneur;
		} catch (SQLException e) {
			throw new ExceptionRequest("Impossible de trouver les " + this.getClass().getName());
		}
	}
	
}
