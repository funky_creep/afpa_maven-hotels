package model.model;

import java.util.HashMap;
import java.util.Map;

public class Collectionneur<T extends Collectionnable> {

	private Map<Integer, T> collection = new HashMap<Integer, T>();
	
	public Map<Integer, T> getCollection() {
		return collection;
	}
	
	public void addItem(T item) {
		collection.put(item.getId(), item);
	}
	
	public T getItem(Integer itemId) {
		return collection.get(itemId);
	}
	
	public int getSize() {
		return collection.size();
	}
}
