package model.model;

import model.dao.DAO;
import model.dao.DAOCategorie;
import model.dao.DAOChambre;
import model.dao.DAOHotel;
import model.dao.DAOTypeHotel;

public class DAOFactory {

	public static final int DAOHOTEL = 0;
	public static final int DAOCATEGORIE = 1;
	public static final int DAOTYPEHOTEL = 2;
	public static final int DAOCHAMBRE = 3;

	public DAOFactory() {
	}
	
	public DAO getDAO(int dao) {
		switch(dao) {
		case DAOHOTEL:
			return new DAOHotel();
		case DAOCATEGORIE:
			return new DAOCategorie();
		case DAOTYPEHOTEL:
			return new DAOTypeHotel();
		case DAOCHAMBRE:
			return new DAOChambre();
		default:
			return null;
		}
	}
	
}
