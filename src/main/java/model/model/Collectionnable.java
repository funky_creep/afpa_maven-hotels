package model.model;

/**
 * Collectionnable est la classe m�re des beans (tables de la base de donn�es), elle permet notamment d'avoir un DAO g�n�rique
 * @author Funky_Creep
 *
 */
public class Collectionnable {

	/**
	 * l'id est la clef primaire d'une table de la base de donn�es
	 */
	private int id;
	private String nom;

	/**
	 * Cr�e un collectionnable ayant un id qui est sa clef primaire
	 * @param id la clef primaire de la table 
	 */
	public Collectionnable(int id, String nom) {
		this.id = id;
		this.nom = nom;
	}
	
	/**
	 * Permet de r�cup�rer l'id d'une table
	 * @return un entier correspondant � l'id de l'�l�ment
	 */
	public int getId() {
		return id;
	}
	
	public String getNom() {
		return nom;
	}
	
}
